### Status: [![Build status](https://ci.appveyor.com/api/projects/status/rkcguhx29yxw4k31?svg=true)](https://ci.appveyor.com/project/ben.howard1/scalesqlpaasmodule)


# What is it?
A Powershell module for scaling an Azure SQL DB, using either pre-existing tags against the Database resource, or by passing in an explicit Servic Objective to scale to.

# How do I use it?

### Using default tags
By default, the function will check the resource for the following tag names:

- ScaleUpSize - when the `ScaleAction` parameter is pass with value `Up`
- ScaleDownSize - when the `ScaleAction` parameter is pass with value `Down`

If present, the function will attempt to scale the database to the size held by that tag.  For example, if the `ScaleUpSize` tag had the value S5, and the database `$PaaSDB1` was currently an S0, and the following command was passed:

```
Invoke-ScaleSqlPaasDatabase -PaasSqlDatabase $PaaSDB1 -ScaleAction Up 
```
... then the database would be scaled to the value held by the `ScaleUpSize` tag.

### Using non-default tags
You can pass the name of the tag to refer to if you do not want to use the default name:
```
Invoke-ScaleSqlPaasDatabase -PaasSqlDatabase $PaaSDB1 -ScaleAction Up -ScaleUpSizeTagName 'ScaleReallyBig'
```
or
```
Invoke-ScaleSqlPaasDatabase -PaasSqlDatabase $PaaSDB1 -ScaleAction Down -ScaleUpSizeTagName 'ScaleNotSoSmall'
```

### Passing an explicit value
To ignore all tags and just pass an explicit value to Service Objective to scale to, you can call:
```
Invoke-ScaleSqlPaasDatabase -PaasSqlDatabase $PaaSDB1 -ServiceObjectiveName 'S6'
```

### Assumptions
- The module Az.Sql or AzureRM.Sql has been installed
- A valid Azure security context has already been etsablished

### Behaviour
The function will check for the current size of the database.  If it is already of the desired size, no action will be taken.

You can use the `-Whatif` switch to see what action the function would take, without atcually altering the resource.