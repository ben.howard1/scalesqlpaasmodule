<#
.SYNOPSIS
Scale an Azure PaaS Database
.DESCRIPTION
Scale an Azure SQL DB, driven by tag values on the resource or by passing ServiceObjective explicitly
.PARAMETER PaasSqlDatabase
.PARAMETER ScaleAction
.PARAMETER ScaleUpSizeTagName
.PARAMETER ScaleDownSizeTagName
.PARAMETER ServiceObjectiveName

.INPUTS

.OUTPUTS
Summary of database copy
.EXAMPLE
# Scale up according to the value held in the default tag name (ScaleUpSize)
Invoke-PaasSqlDatabaseScaling -PaasSqlDatabase 'PaaSDB1' -ScaleAction Up 
.EXAMPLE
# Scale down according to the value held in the default tag name (ScaleDownSize)
Invoke-PaasSqlDatabaseScaling -PaasSqlDatabase $PaaSDB1 -ScaleAction Down 
.EXAMPLE
# Scale up according to the value of the tag supplied 
Invoke-PaasSqlDatabaseScaling -PaasSqlDatabase $PaaSDB1 -ScaleAction Up -ScaleUpSizeTagName 'SuperScaleUpSize'
.EXAMPLE
# Scale to an explicit size
Invoke-PaasSqlDatabaseScaling -PaasSqlDatabase $PaaSDB1 -ServiceObjectiveName 'S6'

.NOTES
It is assumed a valid Azure security context has already been established.
#> 
function Invoke-ScaleSqlPaasDatabase {
    [CmdletBinding(SupportsShouldProcess)]
    param (
        [Parameter(ParameterSetName='ByActionAndTag', Mandatory=$true)]
        [Parameter(ParameterSetName='ByExplicitSize', Mandatory=$true)]
        [psobject] $PaasSqlDatabase,

        [Parameter(ParameterSetName='ByActionAndTag', Mandatory=$true)]
        [validateset ("Up", "Down")]        
        [string] $ScaleAction,
        
        [Parameter(ParameterSetName='ByActionAndTag', Mandatory=$false)]
        [string] $ScaleUpSizeTagName = 'ScaleUpSize',

        [Parameter(ParameterSetName='ByActionAndTag', Mandatory=$false)]
        [string] $ScaleDownSizeTagName = 'ScaleDownSize',

        [Parameter(ParameterSetName='ByExplicitSize', Mandatory=$true)]
        [string] $ServiceObjectiveName
    )

    if ($PSCmdlet.ParameterSetName -eq 'ByExplicitSize') {$RequestedServiceObjectiveName = $ServiceObjectiveName}

    # Make compatibile with Azure RM
    if ((Get-Module -ListAvailable -Name Az.Sql).Count -gt 0) {
        Write-Verbose "Calling Enable-AzureRmAlias"
        Enable-AzureRmAlias
    }
    

    switch ($ScaleAction) {
        
        ("Up") {
            if (($null -ne $PaasSqlDatabase.Tags.$ScaleUpSizeTagName) ) {
                $RequestedServiceObjectiveName = $PaasSqlDatabase.Tags.$ScaleUpSizeTagName
            }
            else {
                Throw "No tag with name '$($ScaleUpSizeTagName)' was found"
            }
        }

        ("Down") {
            if (($null -ne $PaasSqlDatabase.Tags.$ScaleDownSizeTagName) ) {
                $RequestedServiceObjectiveName = $PaasSqlDatabase.Tags.$ScaleDownSizeTagName
            }
            else {
                Throw "No tag with name '$($ScaleDownSizeTagName)' was found"
            }
        }
    }
    
    if ($null -ne $RequestedServiceObjectiveName) {
        if (($PaasSqlDatabase.CurrentServiceObjectiveName -ne $RequestedServiceObjectiveName)) {

            $Msg = ("Scale {0}/{1} from size {2} to {3}"-f $PaasSqlDatabase.ServerName, $PaasSqlDatabase.DatabaseName, $PaasSqlDatabase.CurrentServiceObjectiveName,  $RequestedServiceObjectiveName) 

            if ($PSCmdlet.ShouldProcess($Msg)) {
                
                Set-AzureRMSqlDatabase -DatabaseName $PaasSqlDatabase.DatabaseName `
                                  -ServerName $PaasSqlDatabase.ServerName `
                                  -ResourceGroupName $PaasSqlDatabase.ResourceGroupName  `
                                  -RequestedServiceObjectiveName $RequestedServiceObjectiveName
                
            }
        }
        else {
            Write-Verbose ("Scaling {0}/{1} is not necessary - size is already {2}"-f $PaasSqlDatabase.ServerName, $PaasSqlDatabase.DatabaseName,  $PaasSqlDatabase.Tags.$ScaleDownSizeTagName) 
        }   
    }
}