param(

)

$CommandName = $MyInvocation.MyCommand.Name.Replace(".Tests.ps1", "")
$ModuleBase = (Get-Item $PsScriptRoot).Parent.Parent.FullName


Remove-Module ScaleSqlPaaS -ErrorAction SilentlyContinue
Import-Module "$ModuleBase\ScaleSqlPaaS.psm1" 


InModuleScope ScaleSqlPaaS {

    Describe 'ScaleDBs' {

        BeforeAll{

            function Set-AzureRMSqlDatabase {

            
            }

            
            $TagsDefault = @{'ScaleUpSize'='S5';'ScaleDownSize'='S0'}
            $TagsDifferent = @{'ScaleUpSizeFoo'='S10';'ScaleDownSizeFoo'='S1'}

            $S5PaaSDB = @{
                'ResourceGroupName'='RGName'
                'ServerName'='svr1'
                'DatabaseName'='db1'
                'ResourceId'='/subscriptions/212b7f1a-0ffb-4514-a1ce-e53c7e6bdb95/resourceGroups/rgname/providers/Microsoft.Sql/servers/svr1/databases/db1'
                'Location'='northeurope'
                'Tags'=$TagsDefault
                'CurrentServiceObjectiveName'='S5'
            }
            
            $S0PaaSDB = @{
                'ResourceGroupName'='RGName'
                'ServerName'='svr1'
                'DatabaseName'='db1'
                'ResourceId'='/subscriptions/212b7f1a-0ffb-4514-a1ce-e53c7e6bdb95/resourceGroups/rgname/providers/Microsoft.Sql/servers/svr1/databases/db1'
                'Location'='northeurope'
                'Tags'=$TagsDefault
                'CurrentServiceObjectiveName'='S0'
            }

            $S5PaaSDBDiff = @{
                'ResourceGroupName'='RGName'
                'ServerName'='svr1'
                'DatabaseName'='db1'
                'ResourceId'='/subscriptions/212b7f1a-0ffb-4514-a1ce-e53c7e6bdb95/resourceGroups/rgname/providers/Microsoft.Sql/servers/svr1/databases/db1'
                'Location'='northeurope'
                'Tags'=$TagsDifferent
                'CurrentServiceObjectiveName'='S5'
            }

            $S10PaaSDBDiff = @{
                'ResourceGroupName'='RGName'
                'ServerName'='svr1'
                'DatabaseName'='db1'
                'ResourceId'='/subscriptions/212b7f1a-0ffb-4514-a1ce-e53c7e6bdb95/resourceGroups/rgname/providers/Microsoft.Sql/servers/svr1/databases/db1'
                'Location'='northeurope'
                'Tags'=$TagsDifferent
                'CurrentServiceObjectiveName'='S10'
            }
            
            $S1PaaSDBDiff = @{
                'ResourceGroupName'='RGName'
                'ServerName'='svr1'
                'DatabaseName'='db1'
                'ResourceId'='/subscriptions/212b7f1a-0ffb-4514-a1ce-e53c7e6bdb95/resourceGroups/rgname/providers/Microsoft.Sql/servers/svr1/databases/db1'
                'Location'='northeurope'
                'Tags'=$TagsDifferent
                'CurrentServiceObjectiveName'='S1'
            }            

        }
       

        Context 'DB should scale using default tags' {

            It 'Scale down from S5' {
                Mock -CommandName Set-AzureRMSqlDatabase -MockWith { $S0PaaSDB }

                $Result = Invoke-ScaleSqlPaasDatabase -PaasSqlDatabase $S5PaaSDB -ScaleAction Down

                Assert-MockCalled -CommandName Set-AzureRMSqlDatabase -Times 1 -Scope It
                $Result.CurrentServiceObjectiveName | Should Be 'S0'
            }

            It 'Scale up to S5' {
                Mock -CommandName Set-AzureRMSqlDatabase -MockWith { $S5PaaSDB }

                $Result = Invoke-ScaleSqlPaasDatabase -PaasSqlDatabase $S0PaaSDB -ScaleAction Up

                Assert-MockCalled -CommandName Set-AzureRMSqlDatabase -Times 1 -Scope It
                $Result.CurrentServiceObjectiveName | Should Be 'S5'
            }  

            It 'Do not scale up' {
                Mock -CommandName Set-AzureRMSqlDatabase -MockWith { $S5PaaSDB }

                $Result = Invoke-ScaleSqlPaasDatabase -PaasSqlDatabase $S5PaaSDB -ScaleAction Up

                Assert-MockCalled -CommandName Set-AzureRMSqlDatabase -Times 0 -Scope It
                $Result | Should Be $Null
            }  

            It 'Do not scale down' {
                Mock -CommandName Set-AzureRMSqlDatabase -MockWith { $S0PaaSDB }

                $Result = Invoke-ScaleSqlPaasDatabase -PaasSqlDatabase $S0PaaSDB -ScaleAction Down

                Assert-MockCalled -CommandName Set-AzureRMSqlDatabase -Times 0 -Scope It
                $Result | Should Be $Null
            }  

        }

        Context 'DB should scale using explicit tags' {

            It 'Scale down from S5' {
                Mock -CommandName Set-AzureRMSqlDatabase -MockWith { $S1PaaSDBDiff }

                $Result = Invoke-ScaleSqlPaasDatabase -PaasSqlDatabase $S5PaaSDBDiff -ScaleAction Down -ScaleDownSizeTagName 'ScaleDownSizeFoo'

                Assert-MockCalled -CommandName Set-AzureRMSqlDatabase -Times 1 -Scope It
                $Result.CurrentServiceObjectiveName | Should Be 'S1'
            }

            It 'Scale up to S10' {
                Mock -CommandName Set-AzureRMSqlDatabase -MockWith { $S10PaaSDBDiff }

                $Result = Invoke-ScaleSqlPaasDatabase -PaasSqlDatabase $S1PaaSDBDiff -ScaleAction Up -ScaleUpSizeTagName 'ScaleUpSizeFoo'

                Assert-MockCalled -CommandName Set-AzureRMSqlDatabase -Times 1 -Scope It
                $Result.CurrentServiceObjectiveName | Should Be 'S10'
            }

            It 'Do Not scale up' {
                Mock -CommandName Set-AzureRMSqlDatabase -MockWith { $S10PaaSDBDiff }

                $Result = Invoke-ScaleSqlPaasDatabase -PaasSqlDatabase $S10PaaSDBDiff -ScaleAction Up -ScaleUpSizeTagName 'ScaleUpSizeFoo'

                Assert-MockCalled -CommandName Set-AzureRMSqlDatabase -Times 0 -Scope It
                $Result | Should Be $Null
            }            
            
            It 'Do Not scale down' {
                Mock -CommandName Set-AzureRMSqlDatabase -MockWith { $S1PaaSDBDiff }

                $Result = Invoke-ScaleSqlPaasDatabase -PaasSqlDatabase $S1PaaSDBDiff -ScaleAction Down -ScaleDownSizeTagName 'ScaleDownSizeFoo'

                Assert-MockCalled -CommandName Set-AzureRMSqlDatabase -Times 0 -Scope It
                $Result | Should Be $Null
            }                        
            
        }

        Context 'DB should scale using explicit value' {

            It 'Scale to S0' {
                Mock -CommandName Set-AzureRMSqlDatabase -MockWith { $S0PaaSDB }

                $Result = Invoke-ScaleSqlPaasDatabase -PaasSqlDatabase $S5PaaSDB -ServiceObjectiveName 'S0'

                Assert-MockCalled -CommandName Set-AzureRMSqlDatabase -Times 1 -Scope It
                $Result.CurrentServiceObjectiveName | Should Be 'S0'
            }

            It 'Scale to S10' {
                Mock -CommandName Set-AzureRMSqlDatabase -MockWith { $S10PaaSDBDiff }

                $Result = Invoke-ScaleSqlPaasDatabase -PaasSqlDatabase $S1PaaSDBDiff -ServiceObjectiveName 'S10'

                Assert-MockCalled -CommandName Set-AzureRMSqlDatabase -Times 1 -Scope It
                $Result.CurrentServiceObjectiveName | Should Be 'S10'
            }


            It 'Do not Scale' {
                Mock -CommandName Set-AzureRMSqlDatabase -MockWith { $S0PaaSDB }

                $Result = Invoke-ScaleSqlPaasDatabase -PaasSqlDatabase $S0PaaSDB -ServiceObjectiveName 'S0'

                Assert-MockCalled -CommandName Set-AzureRMSqlDatabase -Times 0 -Scope It
                $Result | Should Be $Null
            }
            
        }        


        Context 'Missing Tags' {

            It 'Missing scale down tag' {
                Mock -CommandName Set-AzureRMSqlDatabase -MockWith { $S1PaaSDBDiff }

                {Invoke-ScaleSqlPaasDatabase -PaasSqlDatabase $S1PaaSDBDiff -ScaleAction Down -ScaleDownSizeTagName 'NotExists'} |
                    Should -Throw  "No tag with name 'NotExists'"                
                
            }

            It 'Missing scale up tag' {
                Mock -CommandName Set-AzureRMSqlDatabase -MockWith { $S1PaaSDBDiff }

                {Invoke-ScaleSqlPaasDatabase -PaasSqlDatabase $S1PaaSDBDiff -ScaleAction Up -ScaleUpSizeTagName 'NotExists'} |
                    Should -Throw  "No tag with name 'NotExists'"                
                
            }            
        }
    }
}        