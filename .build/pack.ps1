param (
    $NuspecPath = '.\ScaleSqlPaas\ScaleSqlPaaS.nuspec',
    [switch] $DoNotIncrementVersion,
    $ExplicitVersion
    )

Set-Location (Split-Path $PSScriptRoot -Parent) 



if (-not $DoNotIncrementVersion.IsPresent -or ($PSBoundParameters.Keys.Contains('ExplicitVersion')) ) {
    [xml] $NuspecXML = Get-Content $NuspecPath

    if ($PSBoundParameters.Keys.Contains('ExplicitVersion')) {
        Write-Host ("Setting version to {0}" -f $ExplicitVersion)
    
        $NuspecXML.package.metadata.version = $ExplicitVersion
    }
    else {
    
        $VersionArr = $($NuspecXML.package.metadata.version).Split('.')
        $VersionAfter = ("{0}.{1}.{2}" -f $VersionArr[0],$VersionArr[1],(([int32]$VersionArr[2])+1))
    
        Write-Host ("Incrementing version to {0}" -f $VersionAfter)
    
        $NuspecXML.package.metadata.version = $VersionAfter
    }
    
    $NuspecXML.Save((Resolve-Path $NuspecPath))
}
else {
    Write-Host "Not changing nuspec version.."
}

& .\.build\NuGet.exe pack (Resolve-Path $NuspecPath) -NoPackageAnalysis